import React from 'react' 
import PropTypes from 'prop-types'
import styled from 'styled-components';

// Create a <Title> react component that renders an <h1> which is
// centered, palevioletred and sized at 1.5em
const Title = styled.h1`
  font-size: 2.5em;
  text-align: center;
  color: deepskyblue;

`;

// Create a <Wrapper> react component that renders a <section> with
// some padding and a papayawhip background
const Wrapper = styled.section`
  margin-top: 1em;
  padding: 1em 4em 2em 4em;
  border-color: gray;
  border-style: solid;
  border-width: 1px;
  border-radius: 10px;
  text-align: center;
  * {
    font-family: 'Courgette';
  }
  *:not h1{
    font-size: 2.5em;
  }
  hr {
    height: 1px;
    border: 1px solid black;
  }
`;

const Consumption = (props) => {
  const n = props.name
  return(
    <div>
      <Wrapper>
        <Title>iLight Stats</Title>
        <hr/>
        <p>Selected module: </p>
        <b>{props.name}</b>
        <p>Current consumption: </p>
        {
          props.name === "All"
          ? 
          <p>{props.current} (kWh)</p>
          : 
          <p>{props.data[n]["cc"]} (kWh)</p>
        }
        <p>Total consumption: </p>
        {
          props.name === "All"
          ?
          <p>{props.current} (kWh)</p>
          : 
          <p>{props.data[n]["tc"]} (kWh)</p>
        }
        <p>Total price: </p>
        {
          props.name === "All"
          ?
          <p>{props.price} (€)</p>
          : 
          <p>{props.data[n]["price"]} (€)</p>
        }
      </Wrapper>
    </div>
  )
}



export default Consumption;