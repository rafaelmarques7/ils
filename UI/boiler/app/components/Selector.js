import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';

// Create a <Title> react component that renders an <h1> which is
// centered, palevioletred and sized at 1.5em
const Title = styled.h1`
  font-size: 2.5em;
  text-align: center;
  color: deepskyblue;

`;

// Create a <Wrapper> react component that renders a <section> with
// some padding and a papayawhip background
const Wrapper = styled.section`
  margin: 1em;
  padding: 1em;
  border-color: gray;
  border-style: solid;
  border-width: 1px;
  border-radius: 10px;
  text-align: center;
  li {
    cursor: pointer;
    list-style-type: none; 
  };
  * {
    font-family: 'Courgette';
  }
  *:not h1{
    font-size: 1.5em;
  }
  hr {
    height: 1px;
    border: 1px solid black;
  }
`;

const Li = styled.li`
  cursor: pointer; 
`;

const Selector = (props) => {

  return (
    <div>
      <Wrapper>
      <Title>Select module</Title>
      <hr/>  
        {props.names.map( (name, i) => 
          <div key={i}>
            <li
              key={name}
              onClick = {() => props.handleClick(name)}
            >{name}</li>
          </div>
        )
      }
      <div>
        <li onClick = {() => props.handleClick("All")}>All modules</li>
      </div>  
    </Wrapper>
    </div>
  )
} 
export default Selector;