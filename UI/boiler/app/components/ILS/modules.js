import React from 'react' 
import styled from 'styled-components'

const redStyle = {
  color: 'red',
}

const Button = styled.button`
  border-color: gray;
  border-style: solid;
  border-width: 1px;
  border-radius: 10px;
`;

class Modules extends React.Component {
  constructor(props){
    super(props)
  }

  render() {
    let content = []
    if (typeof(this.props.data) !== "undefined"){
      content.push(
        <div key={this.props.num}>
        <p>Arduino {this.props.num}</p>
        <hr/>
        <p>current consumption: {this.props.data[this.props.num]["cc"]} (kWh)</p>
        <p>current status: 
          {
            this.props.data[this.props.num]["active"] === 1 
            ?
            <b style={redStyle}>Active</b> 
            :
            <b>Sleeping</b> 
          } 
        </p>
        <p>light intensity: {this.props.data[this.props.num]["lux"]} (%)</p>
        <Button
          onClick={() => this.props.handleClick(this.props.num)}
        >Force</Button>
        </div>
      )
    }
    return(
      <div>
        {content}
      </div>
    )
  }
}

export default Modules;