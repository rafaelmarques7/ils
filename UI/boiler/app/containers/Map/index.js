import React from 'react'
import PropTypes from 'prop-types'
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react'
import styled from 'styled-components'

const mapStyle = {
  margin: '1em 10em 1em 1em',
  padding: '1em 10em 1em 1em',
  borderColor: 'gray',
  borderWidth: '1px',
  borderStyle: 'solid',
  borderRadius: '10px',
  width: '85%',
  height: '90%'
}

export class MapContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      coords: [],
    }
    this.getCoords = this.getCoords.bind(this)
    this.getMarkers = this.getMarkers.bind(this)
    this.renderMapWithMarkers = this.renderMapWithMarkers.bind(this)
    this.renderSimpleMap = this.renderSimpleMap.bind(this)
  }

  componentWillMount() {
    this.getCoords()
  }

  getCoords() {
    let places = []
    if (typeof this.props.data !== "undefined"){
      Object.keys(this.props.data).map((name, i) => {
        var thenum = Object.keys(this.props.data)[i].replace( /^\D+/g, '')
        let obj = {
          "lat": this.props.data[name]["x"],
          "lng": this.props.data[name]["y"],
          "name": thenum,
        }  
          places.push(obj)
        }
      )
      console.log("PLACES: ", places)
      this.setState({coords: places})
    }
  }

  getMarkers() {
    let markerContent = []
    for (var i=0; i < this.state.coords.length; i++) {
      markerContent.push(
          <Marker
            key = {i}
            position = {{
              lat: this.state.coords[i].lat,
              lng: this.state.coords[i].lng}}
            labelStyle={{backgroundColor: "yellow", fontSize: "10px", padding: "5px"}}
            label = {this.state.coords[i]["name"]}
          />
        )
    }
    return markerContent
  }

  renderMapWithMarkers() {
    const markers = this.getMarkers()
    let mapContent = null
    if (this.state.coords.length) {
      if (this.state.coords[0].lat) {
        mapContent =
          <Map
            style={mapStyle}
            google = {this.props.google}
            clickableIcons={false}
            initialCenter={{
              lat: this.state.coords[0].lat,
              lng: this.state.coords[0].lng
            }}
            center={{
              lat: this.state.coords[0].lat,
              lng: this.state.coords[0].lng
            }}
            zoom={11}
            >
            {markers}
          </Map>

      }
    }
    return(<div>{mapContent}</div>)
  }

  renderSimpleMap() {
    return  (
      <Map
        google = {this.props.google}
        style={mapStyle}
        clickableIcons={false}
        initialCenter={{
          lat: 38.77,
          lng: -9.132
        }}
        center={{
          lat: 38.77,
          lng: -9.132
        }}
        zoom={12}
        >
      </Map>
    )
  }

  render() {
    console.log('Map says hi - props: ', this.props)
    console.log('Map says hi - state: ', this.state)
    let content = null
    content = this.renderMapWithMarkers()
    return (
      <div>
      {content}
      </div>
    )
  }
}

MapContainer.defaultProps = {
  data: {}
}


MapContainer.propTypes = {
  data: PropTypes.object
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBMp2rHR1iKSfkCU0R2IGB3OPohVPSNTRY'
})(MapContainer)

