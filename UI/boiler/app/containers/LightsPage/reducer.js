import {fromJS} from 'immutable';

const initialState = fromJS({
  "active": "All",
  "names": [],
});

function lightsReducer(state = {}, action) {
  switch(action.type) {
    case "SET_TOTAL_DATA":
      //console.log("inside reducer, setting total data, response: ", action.response)
      return state
        .set("data", action.response.body.data)
        .set("total", action.response.body.All.price)
        .set("price", action.response.body.All.price)
        .set("current", action.response.body.All.current)
        .set("names", Object.keys(action.response.body.data))
    case "SET_ACTIVE":
      return state 
        .set("active", action.name)
        default:
      return state;
    }

}

export default lightsReducer;
