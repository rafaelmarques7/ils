import React from 'react';
import { reqTotal,
  setActive 
} from './actions';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import { compose } from 'redux';
import {selectTotal, 
  selectPrice, 
  selectCurrent, 
  selectNames, 
  selectActive,
  selectData,
} from './selector';
import  Consumption from '../../components/Consumption';
import Selector  from '../../components/Selector';
import { Container, Row, Col } from 'reactstrap';
import Map from '../Map';
import { wrapper } from 'google-maps-react/dist/GoogleApiComponent';
import styled from 'styled-components'

const Wrapper = styled.section`
  margin-top: 1em;
  padding: 1em 4em 2em 4em;
  border-color: gray;
  border-style: solid;
  border-width: 1px;
  border-radius: 10px;
  text-align: center;
  * {
    font-family: 'Courgette';
  }
  *:not h1{
    font-size: 2.5em;
  }
  hr {
    height: 1px;
    border: 1px solid black;
  }
`;

class LightsPage extends React.Component {
  constructor(props) {
    super(props)
    //this.props.callTotal()
  }
  componentDidMount() {
    setInterval( () => {
      this.props.callTotal()
      }, 5000
    );
  }
  render() {
    console.log("props: ", this.props)
    return(
      <div>
        <Container>
          <Row>
            <Col xs={12} md={12} >
            <Consumption 
                name={this.props.active}  
                total = {this.props.total}
                current = {this.props.current}
                price = {this.props.price}
                data={this.props.data}
            />
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col xs={6} md={6}>
              <Selector 
                names ={this.props.names} 
                handleClick={(name) => this.props.handleSelect(name)}
              />
            </Col>
            <Col xs={6} md={6}>
              <Map 
                data = {this.props.data}
              />
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: selectData(state),
    active: selectActive(state),
    total: selectTotal(state),
    price: selectPrice(state),
    current: selectCurrent(state),
    names: selectNames(state),
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    callTotal: () => dispatch(reqTotal(dispatch)),
    handleSelect: (name) => dispatch(setActive(name))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'query', reducer });

export default compose(
  withReducer,
  withConnect,
)(LightsPage);

