import { createSelector } from 'reselect';

export const selectTotal = (state) => state.get("query").get("total")
export const selectPrice = (state) => state.get("query").get("price")
export const selectCurrent = (state) => state.get("query").get("current")
export const selectNames = (state) => state.get("query").get("names")
export const selectActive = (state) => state.get("query").get("active")
export const selectData = (state) => state.get("query").get("data")