import ajax from 'superagent';
import { error } from 'util';

export const setActive = (name) => {
  console.log("set active was called - name: ", name)
  return {
    type: "SET_ACTIVE",
    name,
  }
}

export const setTotalData = (response) => {
  return {
    type: "SET_TOTAL_DATA",
    response,
  }
}

export function reqTotal(dispatch){
  console.log("reqTotal was called")
  return (dispatch, getState) => {
    const url = "http://cryptic-dusk-26275.herokuapp.com/get/lights"
    ajax.get(url)
    .end((error, response) => {
      if(!error, response) {
        dispatch(setTotalData(response))
      }
    })
  }
}