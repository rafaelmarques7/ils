import { fromJS } from 'immutable'

const initialState = fromJS({
  "1":  {"lux": "0", "active": "0", "cc": "0"},
  "2":  {"lux": "0", "active": "0", "cc": "0"},
  "3":  {"lux": "0", "active": "0", "cc": "0"},
})

function reducer(state=initialState, action) {
  switch(action.type){
    case "SET_API_RESPONSE":
      return state 
        .set("ils", action.response.body)  
    default:
      return state 
  }
}

export default reducer;