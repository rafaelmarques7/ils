import ajax from 'superagent';
import { error } from 'util';


export const setApiResponse = (response) => {
  return {
    type: "SET_API_RESPONSE",
    response
  }
}

export function updateParams(dispatch, id) {
  return (dispatch, getState) => {
    let state = getState()
    var first = state.get("ils").get("ils")["1"]["active"]
    var second = state.get("ils").get("ils")["2"]["active"]
    var third = state.get("ils").get("ils")["3"]["active"]
    if (id === "1"){
      first = "1"
    } else if (id === "2"){
      second = "1"
    } else if(id === "3") {
      third = "1"
    }
    const url = `http://cryptic-dusk-26275.herokuapp.com/post/lights/params&1=${first}&2=${second}&3=${third}`
    ajax.get(url)
    .end((error, response) => {
      if(!error, response) {
        return null;
      }
    })
  }
}

export function callAPI(dispatch){
  return (dispatch, getState) => {
    const url = "http://cryptic-dusk-26275.herokuapp.com/get/lights"
    ajax.get(url)
    .end((error, response) => {
      if(!error, response) {
        dispatch(setApiResponse(response))
      }
    })
  }
}