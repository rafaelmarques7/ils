import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'
import injectReducer from 'utils/injectReducer'
import styled from 'styled-components'
import reducer from './reducer'
import { callAPI, updateParams   } from './actions'
import { getILS } from './selectors'
import Modules from '../../components/ILS/modules'

const Title = styled.h1`
  font-size: 2.5em;
  text-align: center;
  color: deepskyblue;
`;


const Wrapper = styled.section`
  margin-top: 1em;
  padding: 1em 4em 2em 4em;
  border-color: gray;
  border-style: solid;
  border-width: 1px;
  border-radius: 10px;
  text-align: center;
  * {
    font-family: 'Courgette';
  }
  *:not h1{
    font-size: 2.5em;
  }
  hr {
    height: 1px;
    border: 1px solid black;
  }
`;


class ILSPage extends React.Component {
  constructor(props) {
    super(props)
    this.props.callAPI()
  }

  componentDidMount() {
    setInterval( () => {
      this.props.callAPI()
      }, 5000
    );
  }

  render() {
    console.log("props: ", this.props)
    return(
      <div>
        <Container>
          <Wrapper>
            <Row>
              <Col xs={12} md={12}>
                <Title>iLight's Info</Title>
                <hr/>
              </Col>
              <Col xs={4} md={4}>
                <Modules
                  data = {this.props.ilsData}
                  num = {"1"}
                  handleClick = {(num) => this.props.forceParam(num)}
                />
              </Col>
              <Col xs={4} md={4}>
                <Modules
                  data = {this.props.ilsData}
                  num = {"2"}
                  handleClick = {(num) => this.props.forceParam(num)}
                />
              </Col>
              <Col xs={4} md={4}>
                <Modules
                  data = {this.props.ilsData}
                  num = {"3"}
                  handleClick = {(num) => this.props.forceParam(num)}
                />
              </Col>
            </Row>
          </Wrapper>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ilsData: getILS(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    callAPI: () => dispatch(callAPI(dispatch)),
    forceParam: (id) => dispatch(updateParams(dispatch, id)),
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'ils', reducer });

export default compose(
  withReducer,
  withConnect,
)(ILSPage);
