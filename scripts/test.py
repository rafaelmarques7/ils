import threading, time

def main():
  print("main started")
  n = 0
  while True:
    print("starting thread ", str(n))
    n += 1
    threading.Thread(target=f, args=(n, )).start()
    time.sleep(1)

def f(n):
    print("running thread - ", str(n))
    time.sleep(3)
    print("finished thread - ", str(n))

if __name__ == "__main__":
  main()