import requests 
import random 
import time 
import re 

def get_cache():
  url = "https://cryptic-dusk-26275.herokuapp.com/get/lights/"

  resp = requests.get(url)
  data = dict(resp.json()) 
  obj = data["data"]
  return obj

def post_urls(cache):
  url = "https://cryptic-dusk-26275.herokuapp.com/post/lights/"
  data_curr = []
  for name, data in cache.items():
    data_curr.append((name, data["cc"], data["tc"]))
  urls = []
  for data in data_curr:
    name = re.findall('(\d+)', data[0])[0]
    new_cc = int(data[1]) + random.randint(-1,2)
    new_tc = int(data[2]) + random.randint(0,2)
    u = url + "id=" + name + "cc=" + str(new_cc) + "tc=" + str(new_tc)
    urls.append(u)
  return urls

def post_all(urls):
  for url in urls:
    requests.get(url)

def main():
  while True:
    print("initiating update")
    cache = get_cache()
    urls = post_urls(cache)
    #print(urls)
    post_all(urls)
    time.sleep(1)
    print("finised updating,\nResting\n")

if __name__ == "__main__":
  main()