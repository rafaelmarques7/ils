import time 
import json 
import threading 

def write_to_file(name, sleep):
  print("reading file")
  with open('cache.json', 'r') as f:
    obj = json.load(f)
  obj["name"] = name
  with open('cache.json', 'w') as f:
    print("writing to file") 
    f.write(json.dumps(obj))
    if sleep:
      time.sleep(10)

def main():
  var_1, var_2 = "Rafael", "Rafinha"

  #write_to_file(name=var_1, sleep=True)
  #write_to_file(name=var_2, sleep=False)

  threading.Thread(target=write_to_file, args=(var_1,True)).start()
  threading.Thread(target=write_to_file, args=(var_2,False)).start()
if __name__ == "__main__":
  main()