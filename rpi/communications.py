import requests

def postServer(urls):
    for url in urls:
        try:
            resp = requests.get(url)
            if resp.status_code != 200:
                print("error posting url: ", url)
        except:
            print("something failed in postserver")