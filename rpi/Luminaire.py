DEFAULT_MAXIMUM_LUX = 400
DEFAULT_MINIMUM_LUX = 100
DEFAULT_TIME_WAKEDUP = 10 # 10s
DEFAULT_FRONT_NODES_NUMBER = 3
DEFAULT_BACK_NODES_NUMBER = 1

NODE_DETECT_WORD = '$LUM'

class Data:

	def __init__(self, data):
		self.data = data

	# Process data by splitting the string and atributing
	# each parameter to the node data defined by the
	# specified adress
	# Returns the node whose parameters were updated
	# or None if message was not valid (not identified)
	def process(self, nodeList):
		words = self.data.split(sep = ';')
		print(words)
		# if the message identified is a NODE message
		if words[0] == NODE_DETECT_WORD:
			lum_id_ = int(words[1])
			# checks the index on the list of nodes of the
			# node specified by adress addr
			index = self.checkID(nodeList, lum_id_)
			# if there is not an index (the node is not in list)
			# create an instance of that new node, and
			# checks again for the index
			if index is None:
				Node(lum_id_)
				index = self.checkID(nodeList, lum_id_)
			# get the wanted node using index and uptade
			# the node parameters
			node = nodeList[index]
			node.update(detect = int(words[2]), active = int(words[3]), ta = int(words[4]),
			lux_ref = int(words[5]), lux = int(words[6]), dc = int(words[7])*255/100,
			time = int(words[8]), ip_addr = words[9])
			return node
		return None

	# Loops the list of nodes to search the index of the node
	# specified adress, returning the index
	# If the node with that adress is not in the list,
	# return None
	def checkID(self, nodeList, lum_id):
		index = 0
		for node in nodeList:
			if node.lum_id == lum_id: return index
			index += 1
		return None


class Node:

	# Class variable which will contain all the instances
	# created (all the created nodes)
	allNodes = list()

	def __init__(self, lum_id, ip_addr="0.0.0.0", crosswalk = False, mean_occupancy = 0, mean_criminality = 0):

		self.lum_id = lum_id
		self.ip_addr = ip_addr
		self.ip = ip_addr
		self.max_lux = DEFAULT_MAXIMUM_LUX
		self.min_lux = DEFAULT_MINIMUM_LUX
		self.time_wakedUp = DEFAULT_TIME_WAKEDUP
		self.front_nodes = DEFAULT_FRONT_NODES_NUMBER
		self.back_nodes = DEFAULT_BACK_NODES_NUMBER
		self.occupancy = mean_occupancy
		self.criminality = mean_criminality
		self.crosswalk = crosswalk

		self.lux = 0
		self.dc = 0
		self.lux_ref = 0
		self.active = 0
		self.ta = 0

		self.energy = 0
		self.cumEnergy = 0
		self.comfortError = 0
		self.comfortVariance = 0
		self.samples = 0
		self.cumComfortError = 0
		self.cumComfortVariance = 0
		self.cumOccupancy = 0
		self.cumCriminality = 0
		self.lastLux = 0
		self.lastlastLux = 0
		self.lastTime = 0

		Node.allNodes.append(self)

	def __repr__(self):
		return str(self)

	def __str__(self):
		return str(self.ip_addr)

	# Updates node parameters, and then calculates the evaluation
	# metrics and the statistic values
	def update(self, detect, active, ta, lux_ref, lux, dc, time, ip_addr):

		self.lux = lux
		self.lux_ref = lux_ref
		self.dc = dc
		self.active = active
		self.ta = ta

		self.evalMetrics(time)
		self.statisticUpdate(time, detect)
		self.regression()

		self.lastTime = time
		self.ip_addr = ip_addr

	# Calculate the evaluation metrics
	# energy consumption and acumulated energy consumption
	# comfort error and comfort variance
	def evalMetrics(self, time):

		self.computeEnergy(time)
		self.computeComfort()

	def computeEnergy(self, time):

		self.energy = self.dc * (time - self.lastTime)
		self.energy /= 3600
		self.cumEnergy += self.energy

	def computeComfort(self):

		self.samples += 1
		if(self.lux_ref > self.lux):
			self.cumComfortError += (self.lux_ref - self.lux)
		self.comfortError = self.cumComfortError / self.samples
		if self.samples > 2:
			self.cumComfortVariance += (self.lux - 2*self.lastLux + self.lastlastLux)
			self.lastLux = self.lux
			self.lastlastLux = self.lastLux
		self.comfortVariance = self.cumComfortVariance / self.samples

	# Compute statistic values [per hour]
	def statisticUpdate(self, time, detect = False, crime = False):

		self.setDetection(detect)
		self.setCrime(crime)

		self.occupancy = self.cumOccupancy * 3600 / time
		self.criminality = self.cumCriminality * 3600 / time

	def setDetection(self, detect):
		if detect: self.cumOccupancy += 1

	def setCrime(self, crime):
		if crime: self.cumCriminality += 1

	def setCrosswalk(self):
		self.crosswalk = True

	def regression(self):
		#self.time_wakedUp = int(3600/self.occupancy)
		if self.crosswalk: self.min_lux = self.max_lux

	def print_(self):
		print("\nAddress %d \t\t Lux %d \t\t Reference Lux %d \t Duty Cicle %0.2f \t Status %d \t Time %d" % \
            (self.addr, self.lux, self.lux_ref, self.dc, self.active, self.lastTime))
		print("Energy %0.3f \t\t Cum Energy %0.3f \t Comfort Error %0.3f \t Comfort Variance %0.3f" % \
				(self.energy, self.cumEnergy, self.comfortError, self.comfortVariance))
		print("Waked Time %0.3f \t Max Lux %0.3f \t Min Lux %d" % (self.time_wakedUp, self.max_lux, self.min_lux))
		print("Occupancy %0.3f \t Criminality %0.3f \t Crosswalk %d" % (self.occupancy, self.criminality, self.crosswalk))

	@staticmethod
	def printNodeInfo():
		print("lum_id \t ip_addr \t lux \t lux_r \t lux+ \t lux- \t dc \t stat \t\t e \t e+ \t ce \t cv \t\t t_wu \t occ \t crim \t cross \t\t time")
		for node in Node.allNodes:
			print("%2d \t %s \t %3d \t %3d \t %3d \t %3d \t %3d \t %1d \t\t %0.3f \t %0.2f \t %0.2f \t %4.2f \t\t %2d \t %4d \t %d \t %d \t\t %d" % \
					(node.lum_id, node.ip_addr, node.lux, node.lux_ref, node.max_lux, node.min_lux, node.dc, node.active,
					node.energy, node.cumEnergy, node.comfortError, node.comfortVariance, node.time_wakedUp,
					node.occupancy, node.criminality, node.crosswalk, node.lastTime))
