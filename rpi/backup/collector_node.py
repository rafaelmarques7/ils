import socket

UDP_PORT = 2522
UDP_IP = "192.168.137.229"

ARDUINO_IP = "192.168.137.24"
ARDUINO_PORT = 4210

message = "this is a UDP message"

addr_arduino = (ARDUINO_IP, ARDUINO_PORT)

sock = socket.socket(socket.AF_INET, # Internet
             socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))
print "waiting on port: ", UDP_PORT, "with IP: ", UDP_IP

while True:
    #print("ready to receive data")
    #data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print("ready to send data ")
    
    sock.sendto(message.encode('utf-8'),addr_arduino)
    print(message)
    
    print("ready to receive data")
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print(data.decode('utf-8'))

sock.close()
