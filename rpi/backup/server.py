import socket
import sys

class Server:

	def __init__(self, adress = '', port = 10000):

		# CreateaTCP/IP socket
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		# Bind the socket to the port
		server_address = (adress, port)
		self.sock.bind(server_address)
		print ('Starting up on %s port %s' % self.sock.getsockname())
		self.sock.listen(1)

	def run(self, inst):

		while True:

			print ('\n\nWaiting for a connection')
			connection, client_address = self.sock.accept()
			try:
				print ('Connection from', client_address)
				while True:
					data = connection.recv(16)
					decodedData = data.decode('utf-8')
					print('Received "%s" from %s' \
						% (decodedData, client_address))
					if decodedData:
						ans = self.callback(decodedData, inst)
						if ans:
							connection.sendall(ans)
						else:
							data = '-1'.encode('utf-8')
							connection.sendall(data)
					else:
						break
			finally:
				connection.close()

	def receivedMessageStatus(self, data):
		words = data.split(sep = ',')
		if len(words) == 2:
			print ('Client asked information "%s" about Node %s' % (words[0], words[1]))

	def callback(self, receivedMessage, inst):

		if receivedMessage == 'quit':
			sys.exit(1)
		elif receivedMessage[0:3] == '<e>':
			node = self.getNode(inst, receivedMessage)
			if node:
				parameter = str(round(node.cumEnergy,5))
				message = parameter.encode('utf-8')
				return message
		elif receivedMessage[0:4] == '<ce>':
			node = self.getNode(inst, receivedMessage)
			if node:
				parameter = str(round(node.comfortError,5))
				message = parameter.encode('utf-8')
				return message
		elif receivedMessage[0:4] == '<cv>':
			node = self.getNode(inst, receivedMessage)
			if node:
				parameter = str(round(node.comfortVariance,5))
				message = parameter.encode('utf-8')
				return message
		return None

	def getNode(self, inst, message):
		words = message.split(sep = ',')
		for node in inst.allNodes:
			if node.addr == int(words[1]): return node
		return None
		