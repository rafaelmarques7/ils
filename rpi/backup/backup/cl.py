from client import Client

cl = Client(adress = 'localhost', port = 10000)

while True:
	try:
		message = input("\n\nInsert message to send\n")
		cl.connect()
		status = cl.send(message)
		if not status: break
	finally:
		cl.close()

cl.close()