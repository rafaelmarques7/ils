from Luminaire import Node, Data
#from server import Server
#from threading import Thread
from time import sleep
from random import randint
import sys
import socket

UDP_PORT = 2522
UDP_IP = "192.168.137.233"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP,UDP_PORT))

print("waiting on port: " + str(UDP_PORT) + "\t with IP: " + UDP_IP)

#sv = Server(adress = '', port = 10000)

#thread = Thread(target = sv.run, args = (Node, ))
#thread.start()

Node(1)
Node(2)
Node(3, True)
Node(4)
Node(5)
Node(6)
Node(7, True)
Node(8)
Node(9)

while True:

    # $LUM; LUM_ID; DETECT; ACTIVE; TA; REF; LUX; DC; TIME

    print("ready to receive data ...")
    data_received_, addr_received = sock.recvfrom(1024)
    print(addr_received)
    #sockect_receiver = sock.recvfrom(1024)
    (client_ip, client_port) = addr_received
    print("data received: " + data_received_.decode('utf-8'))
    print("from: " + str(client_ip) + "\n")

    data_received = data_received_.decode('utf-8') + ";" + str(client_ip)

    data = Data(data_received)
    # process data received and returns the node
    # whose parameters were updated
    node = data.process(Node.allNodes)
    #if node: node.print()
    Node.printNodeInfo()

    print("ready to send data");


sock.close()
