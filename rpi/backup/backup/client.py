import socket
import sys

class Client:

	def __init__(self, adress, port = 10000):
		self.server_address = (adress, port)

	def connect(self):
		# Create a TCP/IP socket
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Connect the socket to the port where the server is listening
		print ('Connecting to %s port %s' % self.server_address)
		self.sock.connect(self.server_address)

	def send(self, message):

		try:
			print ('Sending "%s"' % message)
			messageBytes = bytes(message, 'utf-8')
			self.sock.sendall(messageBytes)

			if message == 'quit': return None

			amount_received = 0
			amount_expected = len(message)
			while amount_received < amount_expected:
				data = self.sock.recv(16)
				decodedData = data.decode('utf-8')
				amount_received += len(data)
				print ('Received "%s"' % decodedData)
				return True
		finally:
			pass

	def close(self):
		self.sock.close()