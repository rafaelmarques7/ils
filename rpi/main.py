import socket
from Luminaire import Node, Data
from communications import postServer
import threading
import time 

  
class Controller(object):
  """ This class controls the arduinos, that is, it reads their data and acts on it  """
  
  #consts
  UDP_PORT = 2522
  UDP_IP = "192.168.106.1" #"192.168.137.233" #192.168.106.1
  BUFF_SIZE = 1024
  NUM_NODES = 3
  WAIT_TIME = 2

  #initialise, takes no input
  #run main if safe
  def __init__(self, *args, **kwargs):
    self._nodes = [Node(i) for i in range(self.NUM_NODES)]
    self.initSocket()
    if self._safe:
      self._counter = time.time()
      self.run()
  
  #open socket and listen
  def initSocket(self):
    try:
      self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      self._socket.bind((self.UDP_IP,self.UDP_PORT))
      print("waiting on por:\n\t", str(self.UDP_PORT) + ",\n\tIP: ", str(self.UDP_IP))
      self._safe = True 
    except:
      print("ERROR: can not open socket!")
      self._safe = False

  # return Data class Object
  def read_arduino(self):
    data_rec, addr_rev = self._socket.recvfrom(self.BUFF_SIZE)
    (client_ip, client_port) = addr_rec
    data_rec = data_rec.decode('utf-8') + ";" + str(client_ip)
    data = Data(data_rec)
    return data

  # update nodes (locally) based on received data
  def proccesData(self):
    return self._dataRec.process(self._nodes) #substitute for self._nodes
    
  # function to construct urls for server update 
  def constructUrls(self):
    #what do we want to POST ? status, current consumption, (we need ID)  
    urls = []
    url_base = "https://cryptic-dusk-26275.herokuapp.com/post/lights/"
    for node in self._nodes:
      name = node.lum_id                          #force to str
      cc = node.energy                            #force to str
      active = node.active                        #force to str int 0 - 1
      lux = int(100 * node.lux / node.max_lux)    #force to str 
      url = url_base + "id=" + str(name) + "&cc=" + str(cc) + "&lux=" + str(lux) + "&active=" + str(active)
      urls.append(url)
    return urls 

  # invoke communication module to post on API
  def updateServer(self):
    urls = self.constructUrls()
    threading.Thread(target=postServer, args = (urls, )).start()

  # check for USER defined params, to pass to arduino -> change NODE!
  def updateParams(self):
    url = "https://cryptic-dusk-26275.herokuapp.com/get/lights/params"
    try:
      resp = requests.get(url).json()  #something like {id, Bool(defines active)}
      #UPDATE NODES -> FORCE CALL ARDUINO HERE, else next cycle it will overwrite the user settings with default 
      for node_id, active in resp.items():
        for i, node in enumerate(self._nodes):
          if node.lum_id == node_id:
            self._nodes[i].active = active 
      self.callArduino()
    except:
      print("Could not get user parameters from API")
 
  # update arduino
  def callArduino(self):
    pass 

  #inf cycle - receive data, procces it, update server, update arduino 
  def run(self):
    while True:
      self._dataRec = self.read_arduino()               # read
      self._nodes_updated = self.proccesData()          # local updates
      self.callArduino()                                # call arduino
      if time.time() > self._counter + self.WAIT_TIME:  # call server to update params
        self.updateServer()                         
        self.updateParams()
        self._counter = time.time()

    self._socket.close()                                # close socket at end of inf cycle ? lel
      

if __name__ == "__main__":
  ctr = Controller()
  